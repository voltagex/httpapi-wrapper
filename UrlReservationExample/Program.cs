﻿using NullReference.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NullReference.UrlReservationExample
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter a URL prefix (http://*:8080/) to reserve: ");
            var reservation = Console.ReadLine();

            try
            {
                Console.WriteLine("Adding reservation for " + reservation);
                HttpApi.ReserveUrl(reservation);
             
                Console.ReadLine();

            }

            catch (Win32Exception wex)
            {
                if (wex.Message == "The parameter is incorrect")
                {
                    Console.WriteLine("Invalid value for reservation, maybe you forgot the trailing slash?");
                    reservation = null;
                }

                else
                {
                    Console.WriteLine("A cryptic Win32 error ocurred: " + wex.Message);
                }
            }


            finally
            {
                List<string> reservations = HttpApi.GetReservedUrlList().ToList();
                bool wasAdded = reservations.Contains(reservation);

                Console.WriteLine(reservation + ((wasAdded) ? " was added" : " was not added"));

                if (wasAdded)
                {
                    Console.WriteLine("Deleting reservation for " + reservation);
                    HttpApi.DeleteUrl(reservation);
                }
                Console.Read();
            }

        Console.ReadKey();

        }
    }
}
